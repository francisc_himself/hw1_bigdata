import json
import pprint
import re
from pprint import pprint
import argparse
import operator
import platform
import sys

word_dictionary = {}

# Function to retrieve the to-be searched words in tweets
# input: file path
# output: a dictionary of the form word : grade
def dict_from_file(file_path):
	my_dict = {}
	with open(file_path) as file_sentiment:
		for line in file_sentiment:
			# I inferred from Lister that word and grade are separated
			# by 0x09
			entry = line.split('\x09')
			# The action above maked my grades end in CR+LF
			# I removed them with the instruction below
			if platform.system() == 'Linux':
				entry[1] = entry[1][:-2]
			else:
				if platform.system() == 'Windows':
						entry[1] = entry[1][:-1]
			# Add new entry to dictionary
			if entry: my_dict.update({entry[0]: entry[1]})
	return my_dict

def retrieve_twit_grades(verbose):
	grades_dict = dict()
	with open("twitter_data1.txt") as data_file:
		word_dictionary = dict_from_file("sentiment_scores.txt")
		if verbose !=3:
			pprint(word_dictionary)
		for twit_count, line in enumerate(data_file):
			if verbose != 3:
				print "[Current Tweet:]", twit_count
			current_twit_grade = 0;
			# Get each JSON-representing line
			data = json.loads(line)
			# Get the text tag out of each post
			aText = data.get("text")
			# Encode data to UTF-8
			aText = aText.encode("UTF-8")
			for word in aText.split():
				# The regular expression rule I chose is at least 4
				# alphabetical letters in each word
				match = re.search(r'(^[a-zA-Z*\-a-zA-Z*]{4,}$)', word)
				# If-statement after search() tests if it succeeded
				if match:
					if verbose and verbose != 3:
						print '	found ->', match.group()
					ok_word = match.group()
					if ok_word in word_dictionary:
						if verbose and verbose != 3:
							print "	!!! ", ok_word, "IS IN DICT grade = ", word_dictionary[ok_word]
						current_twit_grade += int(word_dictionary[ok_word])
						# else:
						# 	print "no....."
					# else:
					# 	print 'did not find <- ', word
			if verbose != 3:
				print "	[Conclusion:]TWEET #", twit_count, "got GRADE = ", str(current_twit_grade)
			grades_dict[twit_count] = current_twit_grade
		# pprint(grades_dict)
		return grades_dict

def rank_words_by_freq():
	freq_dict = dict()
	with open("twitter_data1.txt") as data_file:
		for twit_count, line in enumerate(data_file):
			# Get each JSON-representing line
			data = json.loads(line)
			# Get the text tag out of each post
			aText = data.get("text")
			# Encode data to UTF-8
			aText = aText.encode("UTF-8")

			for word in aText.split():
				# The regular expression rule I chose is at least 3
				# alphabetical letters in each word
				match = re.search(r'(^[a-zA-Z\-a-zA-Z]{4,}$)', word)
				# If-statement after search() tests if it succeeded
				if match:
					ok_word = match.group()
					# print ok_word
					if not ok_word in freq_dict:
						freq_dict[ok_word] = 1
					else:
					# Word already exists in dictionary
						freq_dict[ok_word] = freq_dict[ok_word]+ 1
	sorted_freq_dict = sorted(freq_dict.items(), key=operator.itemgetter(1))
	sorted_freq_dict.reverse()
	sorted_freq_dict = sorted_freq_dict[:500]
	return sorted_freq_dict

def infer_sentiment_grade():
	new_word_counter = 0
	top_500_not_in_file = dict()
	top_500_occ_dict = dict()
	# Create a dictionary with tweets and their grades
	a_grades_dict = retrieve_twit_grades(3)
	from_file_dict = dict_from_file("sentiment_scores.txt")
	top_500_words = rank_words_by_freq()
	for key, value in top_500_words:
		if key not in from_file_dict:
			top_500_not_in_file[key] = [0, 0, 0]

	with open("twitter_data1.txt") as data_file:
		for twit_count, line in enumerate(data_file):
			# Get each JSON-representing line
			data = json.loads(line)
			# Get the text tag out of each post
			aText = data.get("text")
			# Encode data to UTF-8
			aText = aText.encode("UTF-8")

			for word in aText.split():
				# The regular expression rule I chose is at least 4
				# alphabetical letters in each word
				match = re.search(r'(^[a-zA-Z\-a-zA-Z]{4,}$)', word)
				# If-statement after search() tests if it succeeded
				if match:
					ok_word = match.group()
				# Not in the provided dictionary
					if ok_word in top_500_not_in_file:
						if a_grades_dict[twit_count] < 0:
							# 1st list entry: NEGATIVE occurencies
							top_500_not_in_file[ok_word][0] += 1
						else:
							if a_grades_dict[twit_count] > 0:
								# 2nd list entry: POSITIVE occurencies
								top_500_not_in_file[ok_word][1] += 1
		for key in top_500_not_in_file:
			if top_500_not_in_file[key][0]+top_500_not_in_file[key][1] != 0:
				top_500_not_in_file[key][2] = 10 * float(top_500_not_in_file[key][1])/(top_500_not_in_file[key][0]+top_500_not_in_file[key][1]) -5
				top_500_not_in_file[key][2] = "GRADE = "+str(format(top_500_not_in_file[key][2], '.2f'))
		for key in top_500_not_in_file:
			top_500_not_in_file[key][0] = "(-)" + str(top_500_not_in_file[key][0])
			top_500_not_in_file[key][1] = "(+)" + str(top_500_not_in_file[key][1])
	pprint(top_500_not_in_file)

def happyami():
	print "Processing ....... "
	nb_friends_positive = 0
	nb_friends_negative = 0
	nb_friends_neutral = 0

	sum_friends_positive = 0
	sum_friends_neutral = 0
	sum_friends_negative = 0

	user_dict = dict()
	my_grades_dict = retrieve_twit_grades(3)
	with open("twitter_data1.txt") as data_file:
		for twit_count, line in enumerate(data_file):
				# print "[Current Tweet:]", twit_count
				current_twit_grade = 0
				# Get each JSON-representing line
				data = json.loads(line)
				# Get the text tag out of each post
				user = data.get("user")
				user_id = user.get("id")
				# print "USER ID = ", user_id
				friend_count = user.get("friends_count")
				if not user_id in user_dict:
					user_dict[user_id] = [friend_count, my_grades_dict[twit_count]]
				if my_grades_dict[twit_count] < 0:
					sum_friends_negative += friend_count
					nb_friends_negative += 1
				else:
					if my_grades_dict[twit_count] > 0:
						sum_friends_positive += friend_count
						nb_friends_positive += 1
					else:
						sum_friends_neutral += friend_count
						nb_friends_neutral += 1
		# I propose calculating the average nb of friends for positive, negative, and neutral
		if nb_friends_positive != 0 and sum_friends_positive != 0:
			avg_positive = sum_friends_positive / nb_friends_positive
			print "AVG #of friends for positive ppl(+) = ", avg_positive
		if nb_friends_negative!= 0 and sum_friends_negative != 0:
			avg_negative = sum_friends_negative / nb_friends_negative
			print "AVG #of friends for negative ppl(-) = ", avg_negative
		if nb_friends_neutral != 0 and sum_friends_neutral != 0:
			avg_neutral = sum_friends_neutral / nb_friends_neutral
			print "AVG #of friends for neutral ppl(o) = ", avg_neutral

				# friend_count = user.get("friends_count")
				# print "	", twit_count, " has ", friend_count, " friends and grade ", my_grades_dict[twit_count]

## Program begins here

parser = argparse.ArgumentParser(description="lab1 BIG data")
parser.add_argument ("-grades", action='store_true', help="Retrieve grades of tweets", required=False)
parser.add_argument ("-freq", action='store_true', help="Show word frequency from tweets, ranked", required=False)
parser.add_argument ("-verbose", action='store_true', help="Show prints for debug purpose(grades)", required=False)
parser.add_argument ("-infergrade", action='store_true', help="Inference of grade for words non appearing in database text", required=False)
parser.add_argument ("-happyami", action='store_true', help="Tries infering if the more friends, the more happiness", required=False)


args = parser.parse_args()

if len(sys.argv[1:])==0:
	parser.print_help()

if args.grades:
	if args.verbose:
		retrieve_twit_grades(1)
	else:
		retrieve_twit_grades(0)
if args.freq:
		sorted_freq_dict = rank_words_by_freq()
		print "TOP 500 terms: "
		pprint(sorted_freq_dict)

if args.infergrade:
		infer_sentiment_grade()
if args.happyami:
		happyami()
